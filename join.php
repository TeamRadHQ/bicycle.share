<?php include_once('template/functions.php'); ?>
<?php echo addHead('Join'); ?>
        <form action="" method="post">
            <p>
                <label for="firstName">Name:</label>
                <input placeholder="First name..." name="firstName" type="text">
                <input placeholder="Last name..." name="lastName" type="text">
            </p>
            <p>
                <label for="userName">User name:</label>
                <input placeholder="User name..." name="userName" type="email">
            </p>
            <p>
                <label for="email">Email:</label>
                <input placeholder="Email..." name="email" type="email">
            </p>
            <p>
                <label for="Password">Password:</label>
                <input placeholder="Password..." name="password" type="password">
                <input placeholder="Confirm Password..." name="passwordConfirm" type="password">
            </p>
            <p class="submit">
                <button type="submit">
                    Join Now!
                </button>
            </p>
        </form>
<?php echo addFoot(); ?>
