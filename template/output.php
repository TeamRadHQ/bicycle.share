#!/bin/php -q
<pre>
<?php
/**
 * This script will parse all php files in the project 
 * root directory to flat html files. $pageName.php 
 * will be named $pageName.htm. Files are placed 
 * in bicycle.share/flattened/ and the will be 
 * overwritten every time the script is run.
 */

// Get PHP files from the parent folder.
shell_exec('cd /Users/paulbeynon/Sites/schoolwork/bicycle.share/template');
$pages = glob('../*.{php}', GLOB_BRACE);
$pages = ['index.php','info.php','join.php','map.php'];

shell_exec('rm /Users/paulbeynon/Sites/schoolwork/bicycle.share/flattened/*.htm');
echo "=================================================\n";
echo "\t\tFlatten PHP Files\n";
echo "=================================================\n";
echo "Removed existing pages...\n\n";
echo "Parsing " . count($pages) . " files to '../flattened'...\n\n";
foreach ( $pages as $page ) {
	$page = str_replace('../', '', $page);
	ob_start();
	include('/Users/paulbeynon/Sites/schoolwork/bicycle.share/'.$page);
	$page = str_replace('.php', '.htm', $page);
	$content = str_replace('.php', '.htm', ob_get_contents());
	file_put_contents("/Users/paulbeynon/Sites/schoolwork/bicycle.share/flattened/".$page, $content);
	ob_end_clean();
	echo "\tCreated '$page'...\n";
}
echo "\nAll Done!\n";
?>
</pre>