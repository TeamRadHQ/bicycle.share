<?php
// Global application vars
$siteTitle="Bicycle Share";

/**
 * Outputs the page head and appends the page title
 * @param string $docTitle 	Pass an optional title for the page. 
 *                          This will prepend " - Bicycle Share" 
 *                          if passed. Else it just outputs "Bicycle 
 *                          Share to page.";
 */
function addHead($docTitle=null) {
	// The title of the site.
	global $siteTitle;
	// An inline pageTitle
	$pageTitle=null;
	if ($docTitle) {
		$pageTitle = "<h2>$docTitle</h2>";
		$docTitle .= " - ";
	}
	$docTitle.= $siteTitle;
	return <<<EOT
<!DOCTYPE html>
<html>

<head>
    <!-- Page meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Author" content="Paul Beynon">
    <!-- Polymer -->
    <!-- Polyfill Web Components support for older browsers -->
    <script src="bower_components/webcomponentsjs/webcomponents-lite.min.js"></script>
    <!-- Import element -->
    <link rel="import" href="bower_components/google-map/google-map.html">

    <!-- Application -->
    <title>{$docTitle}</title>
    <link rel="stylesheet" href="css/app.css">
</head>

<body>
    <main>
        <header>
            <h1>{$siteTitle}</h1>
            {$pageTitle}
        </header>
EOT;
	if ($pageTitle) $head.= $pageTitle;
	return $head;
}
/**
 * Returns the application page footer and navigation.
 */
function addFoot() {
	global $siteTitle;
	$footer = "</main>" . addNav();
	$footer.= <<<EOT
	    <footer>
			<p> &copy; 2016 {$siteTitle}
	    </footer>
</body>

</html>
EOT;
	return $footer;
}
function addNav() {
	return <<<EOT
<nav>
    <ul>
        <li>
        	<a title="Join" class="linkJoin" href="join.php">Join</a>
        </li>
        <li>
        	<a title="Home" class="linkHome" href="/">Home</a>
        </li>
        <li>
        	<a title="Settings" class="linkSettings" href="#">Settings</a>
        </li>
        <li>
        	<a title="Map" class="linkMap" href="map.php">Map</a>
        </li>
        <li>
        	<a title="Profile" class="linkProfile" href="#">Profile</a>
        </li>
        <li>
        	<a title="Info" class="linkInfo" href="info.php">Info</a>
        </li>
    </ul>
</nav>
EOT;
}
?>