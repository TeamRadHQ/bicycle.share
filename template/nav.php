<nav>
    <ul>
        <li>
        	<a title="Join" class="linkJoin" href="join.php">Join</a>
        </li>
        <li>
        	<a title="Home" class="linkHome" href="#">Home</a>
        </li>
        <li>
        	<a title="Settings" class="linkSettings" href="#">Settings</a>
        </li>
        <li>
        	<a title="Map" class="linkMap" href="#">Map</a>
        </li>
        <li>
        	<a title="Profile" class="linkProfile" href="#">Profile</a>
        </li>
        <li>
        	<a title="Info" class="linkInfo" href="#">Info</a>
        </li>
    </ul>
</nav>
