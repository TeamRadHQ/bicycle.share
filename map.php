<?php include_once('template/functions.php'); ?>
<?php echo addHead('Melbourne CBD'); ?>
    <google-map latitude="-37.8137569" longitude="144.9611407" zoom="15">
        <google-map-marker latitude="-37.8179746" longitude="144.9668636" draggable="false" title="Federation Square"></google-map-marker>
        <google-map-marker latitude="-37.8182668" longitude="144.9648678" draggable="false" title="Flinders Street Station"></google-map-marker>
        <google-map-marker latitude="-37.8174635" longitude="144.9292856" draggable="false" title="Docklands"></google-map-marker>
        <google-map-marker latitude="-37.8137569" longitude="144.9611407" draggable="false" title="GPO"></google-map-marker>
        <google-map-marker latitude="-37.8165604" longitude="144.9453115" draggable="false" title="Docklands"></google-map-marker>    
        <google-map-marker latitude="-37.8190331" longitude="144.9518478" draggable="false" title="Southern Cross"></google-map-marker>    
        <google-map-marker latitude="-37.807362" longitude="144.963013" draggable="false" title="RMIT University"></google-map-marker>    
        <google-map-marker latitude="-37.811911" longitude="144.973751" draggable="false" title="Parliament"></google-map-marker>    
        <google-map-marker latitude="-37.819771" longitude="144.972316" draggable="false" title="Boathouse Drive"></google-map-marker>    
    </google-map>
<!-- api key = AIzaSyDyX3eBfVbgu_W5SOfn-wPeYD8j1cYOO4s -->
<?php echo addFoot(); ?>
