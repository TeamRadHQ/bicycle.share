#Bicycle Share

This is the repository for the Bicycle Share web design. 

[View a live demonstration](http://teamradhq.com/bicycle.share/)

[View client brief](https://bitbucket.org/TeamRadHQ/bicycle.share/raw/82f13d0d870737dbff2cf9ed2e8472ae2f1ca50f/assets/clientBrief-one-page-layout.pdf)

##Design

The site has been designed using a single template. It has a simple structure with a main content area, and fixed navigation and footer areas. 

A PHP template has been used to generate the outer page area to ensure a consistent look and feel across pages. 

##CSS 3

CSS for this site has been generated using the [Gulp](http://gulpjs.com) build system. Styles have been written using the [Stylus](http://stylus-lang.com) preprocessor and the [Nib](http://nibstyl.us/) mixin collection. 

The CSS is further process using [PostCSS](http://postcss.org) plugins. [Autoprefixer](https://github.com/postcss/autoprefixer) improves cross-browser compatibility and [CSSnano](http://cssnano.co) reduces the size of the CSS by merging style rules and minifying output. 

##Custom Polymer Elements

The [<google-map> custom element set](https://elements.polymer-project.org/elements/google-map) from Google's [Polymer Project](https://www.polymer-project.org/1.0/) have been used to integrate Google Maps. 