var gulp     = require('gulp');
var stylus   = require('gulp-stylus');
var minify   = require('gulp-cssnano');
var prefixer = require('gulp-autoprefixer');

gulp.task('styles', function() {
    gulp.src('css/app.styl')
        .pipe(stylus())
        .pipe(prefixer())
        // .pipe(minify())
        .pipe(gulp.dest('./css/'));
});

gulp.task('watch:styles', function() {
    gulp.watch('**/*.styl', ['styles']);
});
